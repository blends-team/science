Task: Mathematics-dev
Install: false
Description: Debian Science Mathematics-dev packages
 This metapackage will install Debian Science packages which might be
 helpful for development of applications for Mathematics.
 .
 You might also be interested in the science-mathematics metapackage.

Recommends: libnglib-dev
Why: FEA, mesh generator

Suggests: libtet1.5-dev
Why: non-free

Recommends: python3-numpy

Recommends: python3-numexpr

Recommends: libgsl0-dev

Recommends: liblapack-dev, libatlas-base-dev, libblas-dev, libopenblas-dev, libblis-dev

Recommends: python3-cython-blis

Suggests: python3-threadpoolctl

Suggests: libjlapack-java, libmtj-java, libnetlib-java

Suggests:  libcblas-dev

Recommends: libsuperlu-dev
Why: solver

Recommends: libsuperlu-dist-dev

Recommends: libglpk-dev, libglpk-java,
	 libqsopt-ex-dev
Why: linear programming

Recommends: python3-swiglpk

Recommends: libcgal-dev
Why: computational geometry

Recommends: libqhull-dev

Recommends: libmatheval-dev

#Recommends: python-openopt

Recommends: libmatio-dev

Recommends: libblitz0-dev

Recommends: libjama-dev, libtnt-dev, liblip-dev, libranlip-dev
Why: Requested by Aníbal Monsalve Salazar

Recommends: libarpack2-dev, libarpack++2-dev

Recommends: code-saturne-bin, code-saturne-include

Recommends: libarmadillo-dev

Suggests: libitpp-dev

Recommends: libnauty2-dev

Suggests: libtestu01-0-dev

Suggests: sollya, libsollya-dev

Suggests: liblrs-dev

Recommends: libstxxl-dev

Recommends: libnewmat10-dev

Recommends: libmadlib-dev
Why: Mesh adaptation

Recommends: python3-dmsh, python3-meshplex, python3-meshzoo

Recommends: fenics, python3-dolfin, libbasix-dev, python3-basix, libdolfinx-dev, python3-dolfinx, python3-ffcx
Why: Automated Solution of Partial Differential Equations

Recommends: python3-ffc, python3-ufl
Why: compiler for finite element variational forms

Recommends:  petsc-dev
Why: Portable Extensible Toolkit for Scientific Computation

Recommends: slepc-dev
Why: Scalable Library for Eigenvalue Problem Computations

Recommends: python3-petsc4py
Why: Python interface to PETSc

Recommends: python3-slepc4py
Why: Python interface to SLEPc

Suggests: libqrupdate-dev

Recommends: python3-openturns, libopenturns-dev
Why: Uncertainty quantification in numerical simulation

Suggests: openturns-examples

Recommends: libqd-dev
Why: double-double and quad-double precision C++ datatypes

Suggests: cfortran

Recommends: libcvector-dev

Recommends: libhypre-dev

Recommends: libitsol-dev

Recommends: libmuparser-dev

Recommends: libsparskit-dev

Recommends: libspooles-dev

Recommends: libsuitesparse-dev, libsuitesparse-metis-dev

Recommends: libparmetis-dev

Recommends: libscythestat-dev

Recommends: libfreefem++-dev, libfreefem-dev

Recommends: libgivaro-dev

Suggests: givaro-dev-doc, givaro-user-doc

Recommends: fflas-ffpack

Suggests: fflas-ffpack-dev-doc, fflas-ffpack-user-doc

Recommends: liblevmar-dev

Recommends: libpolybori-dev, libpolybori-groebner-dev

Recommends: python3-brial

Recommends: libquadrule-dev

Recommends: libcdd-dev,
         libec-dev,
         libecm-dev,
         libgf2x-dev,
         libiml-dev,
         liblfunction-dev,
         libfplll-dev,
         liblinbox-dev,
         liblrcalc-dev,
         libm4ri-dev,
         libm4rie-dev,
         libratpoints-dev,
         libtachyon-dev,
         libzn-poly-dev,
         libpynac-dev,
         libsymmetrica2-dev
Why: Sage dependencies

Recommends: libprimesieve-dev

Recommends: gap-dev

Recommends: libnfft3-dev

Recommends: libfftw3-dev, fftw-dev, sfftw-dev, python3-pyfftw

Recommends: python3-gpyfft

Recommends: python3-mpi4py-fft

Recommends: libspfft-dev

Recommends: liblbfgs-dev
Why: solver

Recommends: libdogleg-dev
Why: solver

Recommends: libigraph-dev, liblibleidenalg-dev

Recommends: libalglib-dev

Recommends: libeigen3-dev

Recommends: libviennacl-dev, python3-pyviennacl

Recommends: libgnuplot-iostream-dev

Recommends: libqcustomplot-dev

Recommends: libcneartree-dev

Recommends: libmpfrc++-dev

Recommends: libmpfi-dev
Why: gap-float dependencies

Recommends: libscscp1-dev

Recommends: python3-linop

Recommends: libcminpack-dev, minpack-dev

# Sage dependencies should be moved later to sage dependencies
Suggests: libcliquer-dev, libntl-dev

Recommends: libdune-grid-dev
Comment: libdune-common-dev and libdune-geometry-dev are dependencies
 of libdune-grid-dev so there is no point in explicitly putting them
 into the tasks file - these are excluded from the sanity check procedure

Suggests: libdune-istl-dev,
          libdune-localfunctions-dev,
          libdune-functions-dev,
          libdune-grid-glue-dev,
          libdune-pdelab-dev,
          libdune-typetree-dev,
          libdune-uggrid-dev

Recommends: libann-dev, libflann-dev

Recommends: libdouble-conversion-dev

Recommends: libfeel++-dev

Recommends: python3-pynfft

Suggests: python3-numpy-groupies

Recommends: libug-dev

Recommends: libplb-dev

Recommends: libmathicgb-dev

Recommends: libfrobby-dev

Recommends: libmathic-dev

Recommends: python3-dtcwt

Suggests: jsurf-alggeo

Recommends: libsopt-dev
WNPP: 832659

Recommends: python3-gimmik

Recommends: cppad

Recommends: libflint-arb-dev, librw-dev

Recommends: trilinos-all-dev

Recommends: libdeal.ii-dev

Recommends: libp4est-dev

Recommends: coinor-libcoinmp-dev

Recommends: jel-java

Recommends: libjgrapht0.8-java | libjgrapht0.6-java

Recommends: python3-ltfatpy

Recommends: libparsington-java

Recommends: libgismo-dev

Recommends: python3-cvxopt

Suggests: python3-msgpack-numpy

Recommends: python3-asteval

Recommends: python3-brial, libbrial-dev

Recommends: libcombblas-dev

Recommends: libmkl-full-dev

Recommends: libcqrlib-dev

Recommends: python3-cryptominisat

Recommends: python3-cypari2

Recommends: libflint-dev

Recommends: libgemmlowp-dev

Recommends: libbraiding-dev

Recommends: libhomfly-dev

Recommends: libxsmm-dev

Recommends: python3-optlang

Recommends: python3-bumps

Recommends: libmps-dev

Recommends: libsaclib-dev

Recommends: libstopt-dev, python3-stopt

Recommends: libamgcl-dev, python3-amgcl

Recommends: libflame-dev

Recommends: libmagma-dev

Recommends: libmeschach-dev

Recommends: libmeshsdfilter-dev

Recommends: python3-morfessor

Suggests: libbdd-dev

Recommends: libopenmesh-dev

Recommends: libortools-dev, python3-ortools

Recommends: python3-pyemd

Recommends: libsleef-dev

Recommends: libvdt-dev

Recommends: veccore-dev

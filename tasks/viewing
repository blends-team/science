Task: Viewing
Install: true
Description: Debian Science data visualisation packages
 This metapackage will install Debian Science packages related to data
 visualization. You might also be interested in the use::viewing
 debtag.

Recommends: vistrails

Recommends: pdl, gnudatalanguage
Why: IDL-like

Recommends: kst
Why: displaying scientific data

Recommends: ifrit

Recommends: yorick

Recommends: fityk

Recommends: gnuplot, grace, gri, labplot, graphviz, pyxplot, plotdrop, qtiplot

Recommends: python3-guiqwt
Why: collection of widgets based on QWT5 for data visualisation.

Recommends: imview

Recommends: mayavi2

Recommends: g3data

Suggests: libgrits-dev

Suggests: med-imaging

Recommends: gwyddion

Recommends: paraview

Recommends: visit
Why: Used for 3-D visualisation of netCDF datasets.

Recommends: gle-graphics

Recommends: gmsh

Suggests: libgtkmathview-bin

Suggests: jeuclid-mathviewer

Suggests: texlive-pictures

Suggests: pgplot5
Remark: Giza-dev is a DFSG-free replacement for pgplot5.

Recommends: giza-dev
Remark: Giza-dev is a DFSG-free replacement for pgplot5.

Suggests: r-cran-plotrix, r-cran-colorspace, r-cran-labeling, r-cran-scales, r-cran-ggplot2, r-cran-scatterplot3d, r-cran-vioplot, r-cran-shape, r-cran-qqman, r-cran-aplpack

Suggests: r-cran-aplpack

Suggests: findimagedupes, libpuzzle-bin
Why: Those two packages are not necessarily needed for scientific work but a hint for
     their existence might be very reasonable.

Recommends: lybniz

Recommends: 3depict

Recommends: mathgl

Recommends: udav

Suggests: python3-matplotlib

Recommends: matlab2tikz

Recommends: cassandra
Homepage: http://dev.artenum.com/projects/cassandra
License: QPL
Pkg-Description: VTK/Java based 3D Scientific Data Viewer
 Cassandra is a open source scientific data viewer based on VTK. Cassandra
 provides a dynamic interaction with the VTK pipeline and enables to load
 plugins dynamically in order to perform specific tasks in data manipulation
 and visualisation.
 .
 Cassandra is developed by Artenum in the frame of its internal Research and
 Development program and illustrates its skills in scientific visualisation
 and VTK technology. Cassandra has already been used for many applications in
 scientific computing and space environment analysis.

Recommends: pgfplots
Homepage:  http://pgfplots.sourceforge.net/
License: GPLv3
WNPP: 514751
Responsible: OHURA Makoto <ohura@debian.org>
Pkg-Description: TeX package to draw normal and/or logarithmic plots directly in TeX
 This package provides tools to generate plots and labeled axes
 easily. It draws normal plots, logplots and semi-logplots. Axis
 ticks, labels, legends (in case of multiple plots) can be added
 with key-value options. It can cycle through a set of predefined
 line/marker/color specifications. In summary, its purpose is to
 simplify the generation of high-quality function plots,
 especially for use in scientific contexts (logplots).

Suggests: circos, circos-tools

Recommends: feedgnuplot

Recommends: libgnuplot-iostream-dev

Recommends: libqglviewer2

Suggests: libqglviewer-dev

Recommends: python3-gnuplot

Suggests: libcoin-dev, libsoqt520-dev

Suggests: libvtk9-dev, libvtk9-qt-dev, python3-vtk9, libvtk9-java, vtk9-examples

Suggests: python3-pivy

Recommends: ovito

Recommends: scidavis

Recommends: veusz

Recommends: ctioga2

Recommends: python3-seaborn

Recommends: trend

Recommends: python3-pyqtgraph
WNPP: 753590

Recommends: rlplot

Recommends: solvespace

Recommends: gfsview

Suggests: libgts-bin

Recommends: r-cran-sjplot

Recommends: geg

Recommends: camv-rnd

Recommends: open3d-tools, open3d-gui

Recommends: tulip

Recommends: quickplot
